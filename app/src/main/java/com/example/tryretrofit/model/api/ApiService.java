package com.example.tryretrofit.model.api;

import com.example.tryretrofit.model.entity.Repository;
import com.example.tryretrofit.model.entity.User;

import java.util.List;

import retrofit2.http.GET;
import retrofit2.http.Path;

public interface ApiService {
    @GET("/users/{user}")
    io.reactivex.Observable<User> getUser(@Path("user") String username);

    @GET("users/{user}/repos")
    io.reactivex.Observable<List<Repository>> getRepos(@Path("user") String user);
}
