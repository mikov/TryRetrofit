package com.example.tryretrofit.model.entity;

import com.google.gson.annotations.Expose;

public class Repository {
    @Expose String name;

    public String getName() {
        return name;
    }

}
