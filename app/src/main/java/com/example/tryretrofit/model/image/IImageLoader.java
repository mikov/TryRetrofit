package com.example.tryretrofit.model.image;

public interface IImageLoader<T> {
    void loadInto(String url, T container);
}
