package com.example.tryretrofit.model.image.android;

import android.widget.ImageView;

import com.example.tryretrofit.model.image.IImageLoader;
import com.squareup.picasso.Picasso;

public class PicassoImageLoader implements IImageLoader<ImageView> {
    @Override
    public void loadInto(String url, ImageView container) {
        Picasso.get().load(url).into(container);
    }
}
