package com.example.tryretrofit.model.repo;

import com.example.tryretrofit.model.api.ApiHolder;
import com.example.tryretrofit.model.entity.Repository;
import com.example.tryretrofit.model.entity.User;

import java.util.List;

import io.reactivex.Observable;

public class UsersRepo {
    public Observable<User> getUser(String user) {
        return ApiHolder.getApi().getUser(user);
    }

    public Observable<List<Repository>> getRepos(String user) {
        return ApiHolder.getApi().getRepos(user);
    }
}