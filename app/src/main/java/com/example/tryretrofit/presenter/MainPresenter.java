package com.example.tryretrofit.presenter;

import android.annotation.SuppressLint;
import android.util.Log;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.example.tryretrofit.model.entity.Repository;
import com.example.tryretrofit.model.repo.UsersRepo;
import com.example.tryretrofit.view.IListPresenter;
import com.example.tryretrofit.view.IListRowView;
import com.example.tryretrofit.view.MainView;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Scheduler;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

@InjectViewState
public class MainPresenter extends MvpPresenter<MainView> {

    class ListPresenter implements IListPresenter {
        List<String> strings = new ArrayList<>();

        @Override
        public void bindView(IListRowView view) {
            view.setText(strings.get(view.getPos()));
        }

        @Override
        public int getViewCount() {
            return strings.size();
        }

    }

    private ListPresenter listPrestenter = new ListPresenter();

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        getViewState().init();
        List<String> bufList = new ArrayList<>();
        listPrestenter.strings.addAll(bufList);
        getViewState().updateList();
    }

    public ListPresenter getListFiles() {
        return listPrestenter;
    }


    private Scheduler scheduler;
    private UsersRepo usersRepo;

    public MainPresenter(Scheduler scheduler) {
        this.scheduler = scheduler;
        usersRepo = new UsersRepo();
    }

    @SuppressLint("CheckResult")
    public void loadInfo() {
        final String name = "MikovIvan";
        usersRepo.getUser(name)
                .subscribeOn(Schedulers.io())
                .observeOn(scheduler)
                .subscribe(user -> {
                    getViewState().loadImage(user.getAvatarUrl());
                    getViewState().setLoginText(user.getLogin());
                    loadRepo(name);
                }, throwable -> {
                    Timber.e(throwable, "Failed to get user");
                    getViewState().showError(throwable.getMessage());
                    Timber.d(throwable);
                });
    }

    @SuppressLint("CheckResult")
    private void loadRepo(String name) {
        usersRepo.getRepos(name)
                .subscribeOn(Schedulers.io())
                .observeOn(scheduler)
                .subscribe(usersRepo -> {
                    listPrestenter.strings.clear();
                    for (Repository r : usersRepo) {
                        listPrestenter.strings.add(String.valueOf(r.getName()));
                    }
                    getViewState().updateList();
                }, throwable -> Timber.e(throwable, "Failed to get user"));
    }
}
