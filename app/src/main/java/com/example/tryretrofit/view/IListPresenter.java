package com.example.tryretrofit.view;

public interface IListPresenter {
    void bindView(IListRowView view);
    int getViewCount();
}
