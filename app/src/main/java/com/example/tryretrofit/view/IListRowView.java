package com.example.tryretrofit.view;

public interface IListRowView {
    int getPos();
    void setText(String text);
}
