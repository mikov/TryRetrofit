package com.example.tryretrofit.view;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.example.tryretrofit.R;
import com.example.tryretrofit.model.image.IImageLoader;
import com.example.tryretrofit.model.image.android.GlideImageLoader;
import com.example.tryretrofit.presenter.MainPresenter;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;

public class MainActivity extends MvpAppCompatActivity implements MainView {

    @BindView(R.id.iv_avatar) ImageView avatarImageView;
    @BindView(R.id.tv_username) TextView usernameTextView;
    @BindView(R.id.tv_error) TextView errorTextView;
    @BindView(R.id.pb_loading) ProgressBar loadingProgressBar;
    @BindView(R.id.rv) RecyclerView recyclerView;

    IImageLoader<ImageView> imageLoader;
    Adapter adapter;

    @InjectPresenter
    MainPresenter presenter;

    @ProvidePresenter
    public MainPresenter provideMainPresenter() {
        return new MainPresenter(AndroidSchedulers.mainThread());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        imageLoader = new GlideImageLoader();
    }

    @Override
    public void loadImage(String avatarUrl) {
        loadingProgressBar.setVisibility(View.GONE);
        imageLoader.loadInto(avatarUrl, avatarImageView);
    }

    @Override
    public void setLoginText(String login) {
        usernameTextView.setText(login);
    }

    @Override
    public void showError(String message) {
        loadingProgressBar.setVisibility(View.GONE);
        errorTextView.setText(message);
    }

    @Override
    public void init() {
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        adapter = new Adapter(presenter.getListFiles());
        recyclerView.setAdapter(adapter);
        presenter.loadInfo();
    }

    @Override
    public void updateList() {
        adapter.notifyDataSetChanged();
    }
}
