package com.example.tryretrofit.view;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

@StateStrategyType(value = AddToEndSingleStrategy.class)
public interface MainView extends MvpView {

    void init();
    void updateList();
    void loadImage(String avatarUrl);
    void setLoginText(String login);
    void showError(String message);

}
